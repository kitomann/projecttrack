/**
 JavaServer Faces in Action example code, Copyright (C) 2004-2014 Kito D. Mann.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 **/

package org.jia.ptrack.web;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.jia.ptrack.domain.DataStoreException;
import org.jia.ptrack.domain.Project;

@Named
@ViewScoped
public class CreateProjectController extends BaseController {

	private static final long serialVersionUID = 1381820422833534924L;

	private Project project;

	@PostConstruct
	public void init() {
		setProject(new Project());
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void validateUserName(FacesContext facesContext,
			UIComponent component, Object value) throws ValidatorException {
		final String userName = (String) value;
		if (!userName.contains(" ")) {
			throw new ValidatorException(new FacesMessage(
					component.getClientId(),
					"The name must have at least two parts (i.e. Joe Schmoe)"));
		}
	}

	public Outcome add() {
		final FacesContext facesContext = getFacesContext();
		Utils.log(facesContext, "Executing CreateProjectBean.add");

		final Project project = getProject();
		project.setInitialStatus(getStatusCoordinator().getInitialStatus());
		try {
			getProjectCoordinator().add(project);
		} catch (final DataStoreException e) {
			Utils.reportError(facesContext, "A database error has occrred",
					"Error adding project", e);
			return Outcome.ERROR;
		}
		return Outcome.SUCCESS;
	}
}
