package org.jia.ptrack.web;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.jia.ptrack.domain.DataStoreException;
import org.jia.ptrack.domain.ObjectNotFoundException;
import org.jia.ptrack.domain.User;

@Named
@RequestScoped
public class LoginController extends BaseController {

	private static final long serialVersionUID = -3890102438311043031L;

	private String userName;
	private String password;

	public void setUserName(String loginName) {
		userName = loginName;
	}

	public String getUserName() {
		return userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public Outcome login() {
		final FacesContext facesContext = getFacesContext();

		User newUser = null;
		try {
			newUser = getUserCoordinator().getUser(userName, password);
		} catch (final ObjectNotFoundException e) {
			facesContext
			.addMessage(
					null,
					new FacesMessage(
							"Sorry, the name or password you entered is incorrect. Please try again."));
			return null;
		} catch (final DataStoreException d) {
			Utils.reportError(facesContext, "ErrorLoadingUser", d);
			return null;
		}

		getVisit().setUser(newUser);

		return Outcome.SUCCESS;
	}

	public Outcome logout() {
		final FacesContext facesContext = getFacesContext();
		final HttpSession session = (HttpSession) facesContext
				.getExternalContext().getSession(false);

		if (session != null) {
			session.invalidate();
		}

		return Outcome.LOGOUT;
	}
}
