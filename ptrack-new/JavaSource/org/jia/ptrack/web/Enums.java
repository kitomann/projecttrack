/**
   JavaServer Faces in Action example code, Copyright (C) 2004-2014 Kito D. Mann.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 **/

package org.jia.ptrack.web;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.jia.ptrack.domain.ArtifactType;
import org.jia.ptrack.domain.ProjectType;
import org.jia.ptrack.domain.RoleType;

@Named
@ApplicationScoped
public class Enums {
	public ArtifactType[] getArtifacts() {
		return ArtifactType.values();
	}

	public RoleType[] getRoles() {
		return RoleType.values();
	}

	public ProjectType[] getProjectTypes() {
		return ProjectType.values();
	}
}
