package org.jia.ptrack.web;

import java.io.IOException;

import javax.faces.FacesException;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.jia.ptrack.domain.RoleType;

public class AuthorizationPhaseListener implements PhaseListener {

	private static final long serialVersionUID = -5023398793024143161L;

	@Override
	public void afterPhase(PhaseEvent phaseEvent) {
	}

	@Override
	public void beforePhase(PhaseEvent phaseEvent) {

		final FacesContext facesContext = phaseEvent.getFacesContext();
		final ExternalContext externalContext = facesContext
				.getExternalContext();
		final Application application = facesContext.getApplication();
		final String viewId = facesContext.getViewRoot().getViewId();

		if (!Constants.LOGIN_VIEW_ID.equalsIgnoreCase(viewId)) {
			final Visit visit = (Visit) application.getELResolver().getValue(
					facesContext.getELContext(), null, Constants.VISIT_KEY);
			if (visit == null || visit.getUser() == null) {
				Utils.log(facesContext,
						"No user found; redirecting to login page.");
				try {
					externalContext.redirect(externalContext
							.getRequestContextPath() + Constants.LOGIN_VIEW);
				} catch (final IOException e) {
					throw new FacesException(e);
				}
			} else {
				Utils.log(facesContext, "User found; checking credentials");
				final RoleType role = visit.getUser().getRole();
				final FacesMessage message = Utils.checkCredentials(
						facesContext, viewId, role);
				if (message != null) {
					facesContext.addMessage(null, message);
					final NavigationHandler navHandler = application
							.getNavigationHandler();
					navHandler.handleNavigation(facesContext, null,
							Constants.UNAUTHORIZED_OUTCOME);
				} else {
					Utils.log(facesContext, "View id [" + viewId
							+ "] authorized");
				}
			}
		}
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RENDER_RESPONSE;
	}

}
