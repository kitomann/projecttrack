package org.jia.ptrack.web;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.jia.ptrack.domain.DataStoreException;
import org.jia.ptrack.domain.ObjectNotFoundException;
import org.jia.ptrack.domain.Project;

@Named
@ViewScoped
public class UpdateProjectController extends BaseController {

	private static final long serialVersionUID = -5072247687180231947L;
	private String comments;
	private Project project;
	private String projectId;

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectId() {
		return projectId;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void loadProject() {
		if (projectId != null) {
			try {
				project = getProjectCoordinator().get(projectId);
			} catch (final ObjectNotFoundException e) {
				getFacesContext().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_WARN,
								"The project you selected cannot be found",
								"The project is no longer in the data store."));
			} catch (final DataStoreException d) {
				Utils.reportError(getFacesContext(),
						"A database error has occrred",
						"Error loading project", d);
			}
		}
	}

	public Outcome approve() {
		return update(true);
	}

	public Outcome reject() {
		return update(false);
	}

	protected Outcome update(boolean approve) {
		final FacesContext facesContext = getFacesContext();
		Utils.log(facesContext,
				"Executing UpdateProjectBean.update(), approve = " + approve);
		boolean projectFound = true;
		if (project.changeStatus(approve, getVisit().getUser(), comments)) {
			try {
				getProjectCoordinator().update(project);
			} catch (final ObjectNotFoundException e) {
				projectFound = false;
			} catch (final DataStoreException d) {
				Utils.reportError(facesContext,
						"A database error has occurred.",
						"Error updating project.", d);
				return Outcome.ERROR;
			}
		} else {
			Utils.addInvalidStateChangeMessage(facesContext, approve);
			return Outcome.FAILURE;
		}

		if (projectFound == false) {
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"The project you selected cannot be found",
					"The project is no longer in the data store."));
			return Outcome.FAILURE;
		}

		return Outcome.SUCCESS;
	}

}
