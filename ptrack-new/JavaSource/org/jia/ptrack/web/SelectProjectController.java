/**
   JavaServer Faces in Action example code, Copyright (C) 2004-2014 Kito D. Mann.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 **/
package org.jia.ptrack.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.jia.ptrack.domain.DataStoreException;
import org.jia.ptrack.domain.ObjectNotFoundException;
import org.jia.ptrack.domain.Project;

@Named
@ViewScoped
public class SelectProjectController extends BaseController {

	private static final long serialVersionUID = -8298720937779363232L;

	private List<Project> projects;

	public List<Project> getProjects(boolean all) throws DataStoreException {
		if (projects == null) {
			try {
				if (all) {
					projects = getProjectCoordinator().getProjects();
				} else {
					projects = getProjectCoordinator().getProjects(
							getVisit().getUser().getRole());
				}
			} catch (final ObjectNotFoundException e) {
				projects = new ArrayList<Project>(0);
			}
		}
		return projects;
	}
}
