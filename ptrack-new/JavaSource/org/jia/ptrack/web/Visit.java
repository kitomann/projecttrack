/**
   JavaServer Faces in Action example code, Copyright (C) 2004 Kito D. Mann.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 **/

package org.jia.ptrack.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Named;

import org.jia.ptrack.domain.Project;
import org.jia.ptrack.domain.User;

@Named
@SessionScoped
public class Visit implements Serializable {
	private static final long serialVersionUID = -6930365007507613083L;

	private User user;
	private Project currentProject;
	private List<SelectItem> localeItems;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Project getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(Project currentProject) {
		this.currentProject = currentProject;
	}

	public List<SelectItem> getSupportedtLocaleItems() {
		if (localeItems == null) {
			localeItems = new ArrayList<SelectItem>();
			final Application app = FacesContext.getCurrentInstance()
					.getApplication();
			for (final Iterator<Locale> locales = app.getSupportedLocales(); locales
					.hasNext();) {
				final Locale locale = locales.next();
				final SelectItem item = new SelectItem(locale.toString(),
						locale.getDisplayName());
				localeItems.add(item);
			}
			if (localeItems.size() == 0) {
				final Locale defaultLocale = app.getDefaultLocale();
				localeItems.add(new SelectItem(defaultLocale.toString(),
						defaultLocale.getDisplayName()));
			}
		}
		return localeItems;
	}

	public String getLocale() {
		return FacesContext.getCurrentInstance().getViewRoot().getLocale()
				.toString();
	}

	public void setLocale(String locale) {
		FacesContext.getCurrentInstance().getViewRoot()
		.setLocale(new Locale(locale));
	}
}
