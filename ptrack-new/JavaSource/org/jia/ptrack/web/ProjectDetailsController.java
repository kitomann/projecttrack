/**
   JavaServer Faces in Action example code, Copyright (C) 2004-2014 Kito D. Mann.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 **/

package org.jia.ptrack.web;

import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.jia.ptrack.domain.DataStoreException;
import org.jia.ptrack.domain.ObjectNotFoundException;
import org.jia.ptrack.domain.Project;

@Named
@ViewScoped
public class ProjectDetailsController extends BaseController {

	private static final long serialVersionUID = -3095649130278282549L;

	private int firstRow = 0;
	private int rowsPerPage = 5;
	private Project project;
	private String projectId;
	private boolean fromInbox;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public int getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public int getFirstRow() {
		return firstRow;
	}

	public void setFirstRow(int firstRow) {
		this.firstRow = firstRow;
	}

	public boolean getShowNext() {
		return firstRow + rowsPerPage < project.getHistory().size();
	}

	public boolean getShowPrevious() {
		return firstRow - rowsPerPage >= 0;
	}

	public void loadProject() {
		if (projectId != null) {
			try {
				project = getProjectCoordinator().get(projectId);
			} catch (final ObjectNotFoundException e) {
				getFacesContext().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_WARN,
								"The project you selected cannot be found",
								"The project is no longer in the data store."));
			} catch (final DataStoreException d) {
				Utils.reportError(getFacesContext(),
						"A database error has occrred",
						"Error loading project", d);
			}
		}
	}

	public void next(ActionEvent actionEvent) {
		final int newFirst = firstRow + rowsPerPage;
		if (newFirst < project.getHistory().size()) {
			setFirstRow(newFirst);
		}
	}

	public void previous(ActionEvent actionEvent) {
		final int newFirst = firstRow - rowsPerPage;
		if (newFirst >= 0) {
			setFirstRow(newFirst);
		}
	}

	public boolean isFromInbox() {
		return fromInbox;
	}

	public void setFromInbox(boolean fromInbox) {
		this.fromInbox = fromInbox;
	}

}
