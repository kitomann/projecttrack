package org.jia.ptrack.web;

public class Constants {
	// Backing bean keys
	public final static String VISIT_KEY_SCOPE = "sessionScope.";
	public final static String VISIT_KEY = "visit";

	// Model object keys
	public final static String PROJECT_COORDINATOR_SCOPE = "applicationScope.";
	public final static String PROJECT_COORDINATOR_KEY = "projectCoordinator";

	public final static String STATUS_COORDINATOR_SCOPE = "applicationScope.";
	public final static String STATUS_COORDINATOR_KEY = "statusCoordinator";

	public final static String USER_COORDINATOR_SCOPE = "applicationScope.";
	public final static String USER_COORDINATOR_KEY = "userCoordinator";

	// Authorization
	public final static String ORIGINAL_VIEW_SCOPE = "sessionScope";
	public final static String ORIGINAL_VIEW_KEY = "originalTreeId";
	public final static String PROTECTED_DIR = "protected";
	public final static String EDIT_DIR = "protected/edit";
	public final static String LOGIN_VIEW = "/login.jsf";
	public final static String LOGIN_VIEW_ID = "/login.xhtml";
	public final static String LOGGED_OUT_OUTCOME = "logged_out";
	public static final String READWRITE_HOME_VIEW_ID = "/protected/inbox.xhtml";
	public static final String READONLY_HOME_VIEW_ID = "/general/show_all.xhtml";

	public static final String UNAUTHORIZED_OUTCOME = "unauthorized";

	// Action outcomes
	public final static String SUCCESS_READONLY_OUTCOME = "success_readonly";
	public final static String SUCCESS_READWRITE_OUTCOME = "success_readwrite";
	public final static String SUCCESS_OUTCOME = "success";

	public final static String CANCEL_READONLY_OUTCOME = "cancel_readonly";
	public final static String CANCEL_READWRITE_OUTCOME = "cancel_readwrite";
	public final static String CANCEL_OUTCOME = "cancel";

	// Resource bundle keys
	public final static String BUNDLE_BASENAME = "ptrackResources";

	// For JDBC ResultSet example only
	public final static String RESULT_SET_PROJECT_COORDINATOR_SCOPE = "applicationScope.";
	public final static String RESULT_SET_PROJECT_COORDINATOR_KEY = "resultSetProjectCoordinator";

	public static final String HOME_VIEW_ID = "!home";
}
