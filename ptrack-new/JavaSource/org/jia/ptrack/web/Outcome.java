package org.jia.ptrack.web;

public enum Outcome {

	FAILURE, SUCCESS, ERROR, LOGOUT;

	@Override
	public String toString() {
		return name().toLowerCase();
	}
}
