package org.jia.ptrack.web;

import javax.inject.Named;

import org.jia.ptrack.domain.RoleType;

@Named
public class AuthorizationController extends BaseController {

	private static final long serialVersionUID = -4526562645850241626L;

	public boolean isInboxAuthorized() {
		return !getVisit().getUser().getRole().equals(RoleType.UPPER_MANAGER);
	}

	public boolean isCreateNewAuthorized() {
		return getVisit().getUser().getRole().equals(RoleType.PROJECT_MANAGER);
	}

	public boolean isReadOnly() {
		return getVisit().getUser().getRole().equals(RoleType.UPPER_MANAGER);
	}
}
