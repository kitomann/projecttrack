package org.jia.ptrack.web;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.jia.ptrack.domain.ProjectCoordinator;
import org.jia.ptrack.domain.StatusCoordinator;
import org.jia.ptrack.domain.UserCoordinator;

public abstract class BaseController implements Serializable {

	private static final long serialVersionUID = -3075952558295007783L;

	@Inject
	private Visit visit;

	@Inject
	private ProjectCoordinator projectCoordinator;

	@Inject
	private StatusCoordinator statusCoordinator;

	@Inject
	private UserCoordinator userCoordinator;

	// Faces objects

	public FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	public javax.faces.application.Application getApplication() {
		return getFacesContext().getApplication();
	}

	// Application objects

	public Visit getVisit() {
		return visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	// Accessors for business objects

	public ProjectCoordinator getProjectCoordinator() {
		return projectCoordinator;
	}

	public void setProjectCoordinator(ProjectCoordinator projectCoordinator) {
		this.projectCoordinator = projectCoordinator;
	}

	public StatusCoordinator getStatusCoordinator() {
		return statusCoordinator;
	}

	public void setStatusCoordinator(StatusCoordinator statusCoordinator) {
		this.statusCoordinator = statusCoordinator;
	}

	public UserCoordinator getUserCoordinator() {
		return userCoordinator;
	}

	public void setUserCoordinator(UserCoordinator userCoordinator) {
		this.userCoordinator = userCoordinator;
	}
}
