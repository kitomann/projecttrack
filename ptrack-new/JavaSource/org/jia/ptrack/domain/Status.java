package org.jia.ptrack.domain;

import java.io.Serializable;

public interface Status extends Serializable {
	public String getId();

	public String getName();

	public Status getApprovalStatus();

	public Status getRejectionStatus();

	public boolean isInitialState();

	public boolean isFinalState();

	public RoleType getRole();

	public StatusCoordinator getCoordinator();
}
