package org.jia.ptrack.domain;

public interface UserCoordinator {
	public User getUser(String login, String password)
			throws DataStoreException, ObjectNotFoundException;
}
