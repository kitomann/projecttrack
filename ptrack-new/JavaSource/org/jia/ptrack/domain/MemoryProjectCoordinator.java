/**
   JavaServer Faces in Action example code, Copyright (C) 2004-2014 Kito D. Mann.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 **/

package org.jia.ptrack.domain;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named("projectCoordinator")
@ApplicationScoped
public class MemoryProjectCoordinator implements ProjectCoordinator {
	private Map<String, Project> projects;
	private Map<RoleType, List<Project>> projectsByRoles;

	public MemoryProjectCoordinator() {
		try {
			projects = new HashMap<String, Project>();
			projectsByRoles = new HashMap<RoleType, List<Project>>();

			final MemoryStatusCoordinator statusCoordinator = new MemoryStatusCoordinator();
			final User user = new User("proj_mgr", "Joe", "Schmoe", "faces",
					RoleType.PROJECT_MANAGER);
			Date opDate = null;

			Project project = new Project(statusCoordinator.getInitialStatus());
			project.setId("10");
			project.setName("Inventory Manager 2.0");
			// project.setDescription("description");
			project.setInitiatedBy("Rip Van Winkle");
			project.setRequirementsContact("Joan TooBusy");
			project.setRequirementsContactEmail("joan@toobusy.com");
			project.setInitialComments("The first version is horrible and completely unusable. It's time to rewrite it.");
			project.setType(ProjectType.INTERNAL_WEB);
			project.setArtifacts(new ArtifactType[] { ArtifactType.PROPOSAL,
					ArtifactType.PROJECT_PLAN });

			final SimpleDateFormat formatter = new SimpleDateFormat(
					"MM/dd/yyyy hh:mma");
			try {
				opDate = formatter.parse("04/12/2002 04:30pm");
			} catch (final Exception e) {
				e.printStackTrace();
				opDate = new Date();
			}
			project.changeStatus(
					opDate,
					true,
					user,
					"Funding has been approved. The users are excited about the prospect of having something they can use.");

			for (int i = 0; i < 10; i++) {
				try {
					opDate = formatter.parse("08/12/2002 08:30pm");
				} catch (final Exception e) {
					e.printStackTrace();
					opDate = new Date();
				}
				project.changeStatus(
						opDate,
						true,
						user,
						i
						+ " Initial resources have been allocated and a rough plan has been developed.");
			}
			for (int i = 0; i < 10; i++) {
				try {
					opDate = formatter.parse("08/12/2002 08:30pm");
				} catch (final Exception e) {
					e.printStackTrace();
					opDate = new Date();
				}
				project.changeStatus(opDate, false, user, i + " They all lied!");
			}

			add(project);

			project = new Project(statusCoordinator.getInitialStatus());
			project.setId("20");
			project.setName("Test Project #1");
			project.setDescription("description");
			project.setRequirementsContact("Rick Jones");
			project.setRequirementsContactEmail("jones@nowhere.com");
			project.setType(ProjectType.EXTERNAL_DB);
			project.setArtifacts(new ArtifactType[] {
					ArtifactType.ARCHITECTURE, ArtifactType.DEPLOYMENT,
					ArtifactType.MAINTENANCE });
			project.changeStatus(true, user, "blah, blah, blah");
			project.changeStatus(true, user, "blah, blah, blah");
			add(project);

			project = new Project(statusCoordinator.getInitialStatus());
			project.setId("30");
			project.setName("Test Project #2");
			// project.setDescription("This is a description.");
			project.setType(ProjectType.INTERNAL_WEB);
			project.setArtifacts(new ArtifactType[] { ArtifactType.PROPOSAL });
			add(project);
		} catch (final DataStoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Project add(Project project) throws DataStoreException {
		synchronized (this) {
			if (project.getId() == null) {
				project.setId(Integer.toString(projects.size()));
			}
			projects.put(project.getId(), project);
		}
		update(project);

		return project;
	}

	@Override
	public void remove(String id) throws ObjectNotFoundException,
	DataStoreException {
		synchronized (this) {
			final Project project = projects.remove(id);
			if (project != null) {
				final List<Project> projectList = projectsByRoles.get(project
						.getStatus().getRole());
				if (projectList != null) {
					projectList.remove(project);
				}
			}
			if (project == null) {
				throw new ObjectNotFoundException("No object found with id "
						+ id);
			}
		}
	}

	@Override
	public void removeAll() throws DataStoreException {
		synchronized (this) {
			projects.clear();
			projectsByRoles.clear();
		}
	}

	@Override
	public Project get(String id) throws ObjectNotFoundException,
	DataStoreException {
		final Project project = projects.get(id);
		if (project == null) {
			throw new ObjectNotFoundException("No object found with id " + id);
		}
		return project;
	}

	@Override
	public void update(Project project) throws ObjectNotFoundException,
	DataStoreException {
		synchronized (this) {
			projects.values().remove(project);
			projects.put(project.getId(), project);

			final Iterator<RoleType> roles = projectsByRoles.keySet()
					.iterator();
			final Iterator<List<Project>> roleLists = projectsByRoles.values()
					.iterator();
			while (roles.hasNext()) {
				final RoleType role = roles.next();
				final List<Project> roleList = roleLists.next();
				if (roleList.contains(project)) {
					if (project.getStatus().getRole().equals(role)) {
						return; // the role hasn't changed
					} else {
						roleList.remove(project);
					}
				}
			}
			final RoleType role = project.getStatus().getRole();
			List<Project> projectList = projectsByRoles.get(role);
			if (projectList == null) {
				projectList = new ArrayList<Project>();
			}
			projectList.add(project);
			projectsByRoles.put(role, projectList);
		}
	}

	protected List<Project> getProjectsFromMap(Map<String, Project> projectMap)
			throws ObjectNotFoundException, DataStoreException {
		final List<Project> list = new ArrayList<Project>(projectMap.values());
		if (list.isEmpty()) {
			throw new ObjectNotFoundException("No projects found.");
		}
		return list;
	}

	@Override
	public List<Project> getProjects() throws ObjectNotFoundException,
	DataStoreException {
		return getProjectsFromMap(projects);
	}

	@Override
	public List<Project> getProjects(RoleType role)
			throws ObjectNotFoundException, DataStoreException {
		final List<Project> projectList = projectsByRoles.get(role);
		if (projectList == null) {
			throw new ObjectNotFoundException("No projects found for role "
					+ role);
		}
		return new ArrayList<Project>(projectList);
	}
}
