/**
   JavaServer Faces in Action example code, Copyright (C) 2004-13 Kito D. Mann.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 **/
package org.jia.ptrack.domain;

public enum ProjectColumnType {
	NAME(0, "Name"), TYPE(10, "Type"), STATUS(20, "Status"), ROLE(30, "Role");

	private int value;
	private String description;

	ProjectColumnType(int value, String description) {
		setValue(value);
		setDescription(description);
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return description;
	}
}