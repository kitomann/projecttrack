package org.jia.ptrack.domain;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named("statusCoordinator")
@ApplicationScoped
public class MemoryStatusCoordinator implements StatusCoordinator {

	private class BasicStatus implements Status {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3981869151177130023L;
		private final String id;
		private final String name;
		private Status rejectionStatus;
		private Status approvalStatus;
		private boolean initialState;
		private boolean finalState;
		private final RoleType role;
		private final MemoryStatusCoordinator coordinator;

		private BasicStatus(MemoryStatusCoordinator coordinator, String id,
				String name, RoleType role) {
			this.coordinator = coordinator;
			this.id = id;
			this.name = name;
			initialState = false;
			finalState = false;
			this.role = role;
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public RoleType getRole() {
			return role;
		}

		@Override
		public Status getApprovalStatus() {
			return approvalStatus;
		}

		@Override
		public Status getRejectionStatus() {
			return rejectionStatus;
		}

		@Override
		public boolean isFinalState() {
			return finalState;
		}

		@Override
		public boolean isInitialState() {
			return initialState;
		}

		@Override
		public String toString() {
			return name;
		}

		@Override
		public boolean equals(Object other) {
			return ((Status) other).getId().equals(id);
		}

		@Override
		public StatusCoordinator getCoordinator() {
			return coordinator;
		}
	}

	private final Status initialStatus;

	public MemoryStatusCoordinator() {
		initialStatus = initStateMachine();
	}

	private Status initStateMachine() {
		final BasicStatus proposal = new BasicStatus(this, "0", "Proposal",
				RoleType.PROJECT_MANAGER);
		final BasicStatus planning = new BasicStatus(this, "1", "Planning",
				RoleType.PROJECT_MANAGER);
		final BasicStatus analysis = new BasicStatus(this, "2",
				"Requirements/Analysis", RoleType.BUSINESS_ANALYST);
		final BasicStatus architecture = new BasicStatus(this, "3",
				"Architecture", RoleType.DEVELOPMENT_MANAGER);
		final BasicStatus initialDevelopment = new BasicStatus(this, "4",
				"Core Development", RoleType.DEVELOPMENT_MANAGER);
		final BasicStatus betaDeployment = new BasicStatus(this, "5",
				"Beta Deployment", RoleType.SYSTEMS_MANAGER);
		final BasicStatus betaTesting = new BasicStatus(this, "6",
				"Beta Testing", RoleType.QA_MANAGER);
		final BasicStatus finalDevelopment = new BasicStatus(this, "7",
				"Final Development", RoleType.DEVELOPMENT_MANAGER);
		final BasicStatus uatDeployment = new BasicStatus(this, "8",
				"Acceptance Testing Deployment", RoleType.SYSTEMS_MANAGER);
		final BasicStatus uaTesting = new BasicStatus(this, "9",
				"Acceptance Testing", RoleType.QA_MANAGER);
		final BasicStatus productionDeployment = new BasicStatus(this, "10",
				"Production Deployment", RoleType.SYSTEMS_MANAGER);
		final BasicStatus complete = new BasicStatus(this, "11", "Complete",
				RoleType.PROJECT_MANAGER);
		final BasicStatus closed = new BasicStatus(this, "12", "Closed", null);

		proposal.rejectionStatus = proposal;
		proposal.approvalStatus = planning;
		proposal.initialState = true;

		planning.rejectionStatus = proposal;
		planning.approvalStatus = analysis;

		analysis.rejectionStatus = planning;
		analysis.approvalStatus = architecture;

		architecture.rejectionStatus = analysis;
		architecture.approvalStatus = initialDevelopment;

		initialDevelopment.rejectionStatus = architecture;
		initialDevelopment.approvalStatus = betaDeployment;

		betaDeployment.rejectionStatus = initialDevelopment;
		betaDeployment.approvalStatus = betaTesting;

		betaTesting.rejectionStatus = initialDevelopment;
		betaTesting.approvalStatus = finalDevelopment;

		finalDevelopment.rejectionStatus = betaTesting;
		finalDevelopment.approvalStatus = uatDeployment;

		uatDeployment.rejectionStatus = finalDevelopment;
		uatDeployment.approvalStatus = uaTesting;

		uaTesting.rejectionStatus = uatDeployment;
		uaTesting.approvalStatus = productionDeployment;

		productionDeployment.rejectionStatus = finalDevelopment;
		productionDeployment.approvalStatus = complete;

		complete.rejectionStatus = productionDeployment;
		complete.approvalStatus = closed;

		closed.rejectionStatus = closed;
		closed.approvalStatus = closed;
		closed.finalState = true;

		return proposal;
	}

	@Override
	public Status getInitialStatus() {
		return initialStatus;
	}

	@Override
	public boolean isValidStateChange(Status status, boolean approve) {
		if (approve) {
			return !status.isFinalState();
		} else {
			return !status.isInitialState() && !status.isFinalState();
		}
	}
}
