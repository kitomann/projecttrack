package org.jia.ptrack.domain;

import java.util.List;

public interface ProjectCoordinator {
	public Project add(Project project) throws DataStoreException;

	public void remove(String id) throws ObjectNotFoundException,
	DataStoreException;

	public void removeAll() throws DataStoreException;

	public Project get(String id) throws ObjectNotFoundException,
	DataStoreException;

	public void update(Project project) throws ObjectNotFoundException,
	DataStoreException;

	public List<Project> getProjects() throws ObjectNotFoundException,
	DataStoreException;

	public List<Project> getProjects(RoleType role)
			throws ObjectNotFoundException, DataStoreException;
}
