package org.jia.ptrack.domain;

public enum ArtifactType {
	PROPOSAL(0, "Proposal document"), PROJECT_PLAN(3, "Project plan"), ARCHITECTURE(
			5, "Architecture specification"), TEST(10, "Test plan"), DEPLOYMENT(
					15, "Deployment guidelines"), MAINTENANCE(20,
							"Maintenance documentation"), USER(25, "User documentation");

	private int value;
	private String description;

	ArtifactType(int value, String description) {
		setValue(value);
		setDescription(description);
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return description;
	}
}
