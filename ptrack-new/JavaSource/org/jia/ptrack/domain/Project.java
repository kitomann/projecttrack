package org.jia.ptrack.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.Future;

import org.jia.ptrack.domain.validation.Email1;

public class Project implements Serializable {

	private static final long serialVersionUID = 9132317118071206063L;

	private String name;
	private String initiatedBy;
	private String requirementsContact;
	private String description;
	private List<Operation> operationHistory;
	private ArtifactType[] artifacts;
	private ProjectType type;
	private String id;
	private Status status;
	private String initialComments;

	@Email1
	private String requirementsContactEmail;

	@Future
	private Date startDate;
	@Future
	private Date dueDate;
	private Long budget;

	public Project() {
		operationHistory = new ArrayList<Operation>();
		artifacts = new ArtifactType[] {};
	}

	public Project(Status initialStatus) {
		this();
		setInitialStatus(initialStatus);
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}

	public String getInitiatedBy() {
		return initiatedBy;
	}

	public void setRequirementsContact(String requirementsContact) {
		this.requirementsContact = requirementsContact;
	}

	public String getRequirementsContact() {
		return requirementsContact;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public List<Operation> getHistory() {
		return Collections.unmodifiableList(operationHistory);
	}

	public void setArtifacts(ArtifactType[] artifacts) {
		this.artifacts = artifacts;
	}

	public ArtifactType[] getArtifacts() {
		return artifacts;
	}

	public void setType(ProjectType type) {
		this.type = type;
	}

	public ProjectType getType() {
		return type;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public Status getStatus() {
		return status;
	}

	public synchronized boolean changeStatus(boolean approve, User user,
			String comments) {
		return changeStatus(new Date(), approve, user, comments);
	}

	public synchronized boolean changeStatus(Date date, boolean approve,
			User user, String comments) {
		if (status == null) {
			throw new NullPointerException(
					"The initialStatus property must be "
							+ "set before the status can be changed.");
		}

		if (!status.getCoordinator().isValidStateChange(status, approve)) {
			return false;
		}

		final Status fromStatus = status;
		Status toStatus = null;

		if (approve) {
			toStatus = status.getApprovalStatus();
		} else {
			toStatus = status.getRejectionStatus();
		}

		final Operation newAction = new Operation(date, user, fromStatus,
				toStatus, comments);
		operationHistory.add(newAction);
		status = toStatus;
		return true;
	}

	public void setInitialStatus(Status initialStatus) {
		if (status == null) {
			status = initialStatus;
		}
	}

	public Map<String, String> getIdMap() {
		final Map<String, String> idMap = new HashMap<String, String>();
		idMap.put(name, id);
		return idMap;
	}

	public void setInitialComments(String initialComments) {
		this.initialComments = initialComments;
	}

	public String getInitialComments() {
		return initialComments;
	}

	@Override
	public String toString() {
		return name;
	}

	public String getRequirementsContactEmail() {
		return requirementsContactEmail;
	}

	public void setRequirementsContactEmail(String requirementsContactEmail) {
		this.requirementsContactEmail = requirementsContactEmail;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Long getBudget() {
		return budget;
	}

	public void setBudget(Long budget) {
		this.budget = budget;
	}
}
