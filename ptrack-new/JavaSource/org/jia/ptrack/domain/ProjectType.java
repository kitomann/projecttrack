package org.jia.ptrack.domain;

public enum ProjectType {
	UNKNOWN(-1, "Unknown"), EXTERNAL_WEB(0, "External Web Application"), INTERNAL_WEB(
			5, "Internal Web Application"), EXTERNAL_DB(10, "External Database"), INTERANL_DB(
					15, "Internal Database"), EXTERNAL_DESKTOP(20,
							"External Desktop Application"), INTERNAL_DESKTOP(25,
									"Internal Desktop Application");

	private int value;
	private String description;

	ProjectType(int value, String description) {
		setValue(value);
		setDescription(description);
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return description;
	}
}
