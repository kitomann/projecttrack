package org.jia.ptrack.domain;

public enum RoleType {
	UPPER_MANAGER(0, "Upper Manager"), PROJECT_MANAGER(10, "Project Manager"), BUSINESS_ANALYST(
			20, "Business Analyst"), DEVELOPMENT_MANAGER(30,
					"Development Manager"), SYSTEMS_MANAGER(40, "Systems Manager"), QA_MANAGER(
							50, "QA Manager");

	private int value;
	private String description;

	RoleType(int value, String description) {
		setValue(value);
		setDescription(description);
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return description;
	}
}
