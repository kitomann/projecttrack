package org.jia.ptrack.domain;

public class ObjectNotFoundException extends DataStoreException {

	private static final long serialVersionUID = -3711369056546253138L;

	public ObjectNotFoundException() {
		super();
	}

	public ObjectNotFoundException(String message) {
		super(message);
	}

	public ObjectNotFoundException(Exception e) {
		super(e);
	}

	public ObjectNotFoundException(String message, Exception e) {
		super(message, e);
	}

}
