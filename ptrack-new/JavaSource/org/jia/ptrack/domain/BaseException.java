package org.jia.ptrack.domain;

/**
 * The base class for all exceptions. This class can wrap other exceptions.
 *
 * @author Kito D. Mann
 */
public class BaseException extends java.lang.Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7947066219589378142L;
	private String message = "";
	private Exception exception = null;

	public BaseException() {
		super();
	}

	public BaseException(String message) {
		super();
		this.message = message;
		exception = null;
	}

	public BaseException(Exception e) {
		super();
		message = this.getClass().getName();
		exception = e;
	}

	public BaseException(String message, Exception e) {
		super();
		this.message = message;
		exception = e;
	}

	@Override
	public String getMessage() {
		if ((message == null || message.length() == 0) && exception != null) {
			return exception.getMessage();
		} else {
			return message;
		}
	}

	public Exception getException() {
		return exception;
	}

	@Override
	public String toString() {
		return getMessage();
	}

	@Override
	public void printStackTrace() {
		super.printStackTrace();
		if (exception != null) {
			System.err.println();
			System.err.println("Embedded exception:");
			exception.printStackTrace();
		}
	}

	@Override
	public void printStackTrace(java.io.PrintStream s) {
		super.printStackTrace(s);
		if (exception != null) {
			s.println();
			s.println("Embedded exception:");
			exception.printStackTrace(s);
		}
	}

	@Override
	public void printStackTrace(java.io.PrintWriter s) {
		super.printStackTrace(s);
		if (exception != null) {
			s.println();
			s.println("Embedded exception:");
			exception.printStackTrace(s);
		}
	}

}
