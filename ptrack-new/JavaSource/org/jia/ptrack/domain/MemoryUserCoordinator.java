package org.jia.ptrack.domain;

import java.util.Hashtable;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named("userCoordinator")
@ApplicationScoped
public class MemoryUserCoordinator implements UserCoordinator {

	private final Map<String, User> users;

	public MemoryUserCoordinator() {
		users = new Hashtable<String, User>();
		init();
	}

	private void init() {
		User user = null;

		user = new User("upper_mgr", "Casey", "Langer", "faces",
				RoleType.UPPER_MANAGER);
		users.put(user.getLogin(), user);

		user = new User("proj_mgr", "Sean", "Sullivan", "faces",
				RoleType.PROJECT_MANAGER);
		users.put(user.getLogin(), user);

		user = new User("analyst", "Marvin", "Walton", "faces",
				RoleType.BUSINESS_ANALYST);
		users.put(user.getLogin(), user);

		user = new User("dev_mgr", "Devora", "Shapiro", "faces",
				RoleType.DEVELOPMENT_MANAGER);
		users.put(user.getLogin(), user);

		user = new User("qa_mgr", "Tracey", "Burroughs", "faces",
				RoleType.QA_MANAGER);
		users.put(user.getLogin(), user);

		user = new User("sys_mgr", "Ed", "LaCalle", "faces",
				RoleType.SYSTEMS_MANAGER);
		users.put(user.getLogin(), user);
	}

	@Override
	public User getUser(String login, String password)
			throws DataStoreException, ObjectNotFoundException {
		final User user = users.get(login);

		if (user != null && user.getPassword().equals(password)) {
			return user;
		} else {
			throw new ObjectNotFoundException(
					"No user found with that name and password");
		}
	}

	public void addUser(User user) throws DataStoreException {
		users.put(user.getLogin(), user);
	}
}
