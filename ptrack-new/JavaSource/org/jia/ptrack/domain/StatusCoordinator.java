package org.jia.ptrack.domain;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author unascribed
 * @version 1.0
 */

public interface StatusCoordinator {
	public Status getInitialStatus();

	public boolean isValidStateChange(Status status, boolean approve);
}
