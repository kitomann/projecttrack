# Sample code for JSF 2.2 in Action / JSF 2.2 Deep Dive presentations #
## Kito D. Mann, JavaOne 2014

There are two Eclipse projects -- ptrack-new and acme-layout. The first is the actual sample applicaiton. You can see JSF HTML5 attributes in login.xhtml. The second project is acme-layout, which uses the Resoure Library Contacts feature. You will need to build this project and add it to the WEB-INF/lib directory of the ptrack-new WAR deployment to see it in action.

Contact: @kito99

Links: http://www.virtua.com, http://www.jsfcentral.com, http://enterprisejavanews.com